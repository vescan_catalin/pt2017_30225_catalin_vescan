package queue;

import java.awt.Graphics;
import java.awt.Rectangle;

public class Entity {

	public int coordx, coordy, serverSize = 50, clientSize = 30, bandSize = 30;
	
	public int getX() {
		return coordx;
	}
	
	public int getY() {
		return coordy;
	}
	
	public void setPosition(int x, int y) {
		coordx = x;
		coordy = y;
	}
	
	public void move(int dirx, int diry) {
		coordx += dirx;
		coordy += diry;
	}
	
	public Rectangle rectangle() {
		return new Rectangle(coordx, coordy, serverSize, serverSize) ;
	}
	
	// drawing the servers
	public void paintServer(Graphics g) {
		g.fillRect(coordx, coordy, serverSize, serverSize);
	}
	
	// drawing the bands
	public void paintBands(Graphics g) {
		g.fillRect(coordx, coordy, bandSize, bandSize * 7);
	}
	
	// drawing the clients
	public void paintClients(Graphics g) {
		g.fillOval(coordx, coordy, clientSize, clientSize);
	}

}
