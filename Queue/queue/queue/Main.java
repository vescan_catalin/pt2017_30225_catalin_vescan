package queue;

import javax.swing.JFrame;

public class Main {

	JFrame frame;
	MyRunnable panel = new MyRunnable();
	
	public Main() {
		// window
		frame = new JFrame(" Queue ");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(MyRunnable.width, MyRunnable.height);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		
		frame.add(panel);
		
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {

		new Main();

	}

}
