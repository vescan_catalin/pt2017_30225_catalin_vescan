package queue;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class MyRunnable extends JPanel implements ActionListener, Runnable{

	private static final long serialVersionUID = 1L;

	public static final int width = 800, height = 600;
	public int dirx, diry, waitingTime, waitingTime2, waitingTime3, listSize;
	public boolean validate;
	
	Random time = new Random();
	Entity server1, server2, server3, client, client3, seller1, seller2, seller3, band1, band2, band3;
	ArrayList<Entity> clients, clients2, clients3;
	Thread thread;
	
	JButton addClient, start, stop;
	JTextArea serviceTime;
	JScrollPane scroll;
	
	public MyRunnable() {
		thread = new Thread(this);
		thread.start();
		setPreferredSize(new Dimension(width, height - 100));
		setFocusable(true);
		setLayout(null);
		start = new JButton("start");
		start.setBounds(680, 10, 100, 30);
		add(start);
		stop = new JButton("stop");
		stop.setBounds(680, 50, 100, 30);
		add(stop);
		addClient = new JButton("add client");
		addClient.setBounds(10, 10, 100, 30);
		add(addClient);
		serviceTime = new JTextArea(5, 10);
		serviceTime.setEditable(false);
		scroll = new JScrollPane(serviceTime);
		scroll.setBounds(150, 10, 500, 80);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		add(scroll);
		
		start.addActionListener(this);
		stop.addActionListener(this);
		addClient.addActionListener(this);
	}
	
	public void body() {	
		// create server
		server1 = new Entity();
		server1.setPosition(width - 150, height / 3);
		server2 = new Entity();
		server2.setPosition(server1.getX() - 250, server1.getY());	
		server3 = new Entity();
		server3.setPosition(server2.getX() - 250, server2.getY());
		
		// create seller
		seller1 = new Entity();
		seller1.setPosition(server1.getX() + 10, server1.getY() + 10);
		seller2 = new Entity();
		seller2.setPosition(server2.getX() + 10, server2.getY() + 10);
		seller3 = new Entity();
		seller3.setPosition(server3.getX() + 10, server3.getY() + 10);
		
		// create band
		band1 = new Entity();
		band1.setPosition(server1.getX(), server1.getY() + band1.serverSize);
		band2 = new Entity();
		band2.setPosition(server2.getX(), server2.getY() + band2.serverSize);
		band3 = new Entity();
		band3.setPosition(server3.getX(), server3.getY() + band3.serverSize);
		
		// create clients
		clients3 = new ArrayList<Entity>();
		clients2 = new ArrayList<Entity>();
		clients = new ArrayList<Entity>();
		client = new Entity();
		client3 = new Entity();
		// distance between server and client = clientSize + 10 = clientServer
		client.setPosition(server1.getX() - client.serverSize, server1.getY() + 300);
		clients.add(client);
		for(int i = 1; i < 4; i++) {
			Entity part = new Entity();
			part.setPosition(client.getX(), client.getY() + 2 * i * client.clientSize);
			clients.add(part);			
		}
		client3.setPosition(server3.getX() - client3.serverSize, server3.getY() + 300);
		clients3.add(client3);
		for(int i = 1; i < 3; i++) {
			Entity part = new Entity();
			part.setPosition(client3.getX(), client3.getY() + 2 * i * client3.clientSize);
			clients3.add(part);
		}
	}
	
	// create new items / start / stop
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == start)	validate = true;
		if(e.getSource() == stop)	validate = false;
		if(e.getSource() == addClient) {
			for(int i = 0; i < 5; i++) {
				Entity part = new Entity();
				part.setPosition(clients.get(clients.size()-1).getX(), clients.get(clients.size()-1).getY() + 2 * client.clientSize);
				if(clients.get(clients.size() - 1).getY() < 650)
					clients.add(part);
				Entity part3 = new Entity();
				part3.setPosition(clients3.get(clients3.size()-1).getX(), clients3.get(clients3.size()-1).getY() + 2 * client.clientSize);
				if(clients3.get(clients3.size()-1).getY() < 650)
					clients3.add(part3);
			}
			listSize = clients.size();
		}
	}
	
	public void move(int i) {
		diry = 0; dirx = 0; 
		waitingTime = time.nextInt(500);
		waitingTime2 = time.nextInt(250);
		waitingTime3 = time.nextInt(500);
		
		// first que
		try {
			if(clients.get(i).getY() == server1.getY())
				try {
					Thread.sleep(10 + waitingTime);
					serviceTime.append("client " +(i + 1) +" need " +waitingTime +" time units.\t\t queue 1\n");
				} catch (InterruptedException e) {
				}			
			if(clients.get(i).getY() > server1.getY() - 80) {
					diry -= 10;
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
					}
					clients.get(i).move(0, diry);
					diry = 0;
				}
			if(clients.get(i).getY() == server1.getY() - 80) {
				dirx -= 10;
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
				}
				clients.get(i).move(dirx, 0);
				dirx = 0;
			}
		} catch(Exception e) {
		}
		
		// second queue
		try {
			if(listSize > 8 && clients.get(i).getY() == 500 && clients.get(i-1).getY() == 440) {
				clients2.add(clients.get(i));
				clients.remove(i);
			}
		} catch(Exception e) {
		}
		
		divideQueue(i);
		
		// third queue
		try {
			if(clients3.get(i).getY() == server3.getY())
				try {
				Thread.sleep(10 + waitingTime);
				serviceTime.append("client " +(i + 1) +" need " +waitingTime3 +" time units.\t\t queue 3\n");
				} catch (InterruptedException e) {
				}			
			if(clients3.get(i).getY() > server3.getY() - 80) {
					diry -= 10;
					clients3.get(i).move(0, diry);
					diry = 0;
				}
			if(clients3.get(i).getY() == server3.getY() - 80) {
				dirx -= 10;
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
				}
				clients3.get(i).move(dirx, 0);
				dirx = 0;
			}
		} catch(Exception e) {
		}
		
		try {
			if(listSize > 8 && clients3.get(i).getY() == 500 && clients3.get(i-1).getY() == 440) {
				clients2.add(clients3.get(i));
				clients3.remove(i);
			}
		} catch(Exception e) {
		}
	}
	
	public void divideQueue(int i) {
		try {
			if(clients2.get(i).getY() == server2.getY()) {
				try {
					Thread.sleep(10 + waitingTime2);
					serviceTime.append("client " +(i + 1) +" need " +waitingTime2 +" time units.\t\t queue 2\n");
				} catch (InterruptedException e) {
				}	
			}
			if(clients2.get(i).getX() > server2.getX() - client.serverSize) {
				dirx -= 15;
				clients2.get(i).move(dirx, 0);
				dirx = 0;
			}
			if(clients2.get(i).getX() < server2.getX() - client.serverSize) {
				dirx += 10;
				clients2.get(i).move(dirx, 0);
				dirx = 0;
			}
			if(clients2.get(i).getY() > server2.getY() - 80 && clients2.get(i).getX() == server2.getX() - client.serverSize) {
				diry -= 10;
				clients2.get(i).move(0, diry);
				diry = 0;
			}
			for(i = 0; i < clients2.size(); i++)
				if(clients2.get(i).getY() == server2.getY() - 80) {
					dirx -= 10;
					clients2.get(i).move(dirx, 0);
					dirx = 0;
				}	
		} catch(Exception e) {
		}
	}
	
	public void move2(){
		for(int i = 0; i < clients.size(); i++)
			move(i);
	}
	
	public void run() {
		body();

		while(true) {
			if(validate) 
				move2();
			repaint();
		}
	}

	public void paint(Graphics g) {
		super.paint(g);
		
		// set background color
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(0, 100, width, height - 100);
		
		// set servers color
		g.setColor(Color.BLUE);
		server1.paintServer(g);
		server2.paintServer(g);
		server3.paintServer(g);
		
		// set seller color
		g.setColor(Color.GREEN);
		seller1.paintClients(g);
		seller2.paintClients(g);
		seller3.paintClients(g);
		
		// set servers bands color
		g.setColor(Color.BLACK);
		band1.paintBands(g);
		band2.paintBands(g);
		band3.paintBands(g);
		
		// set clients color
		g.setColor(Color.RED);
		for(Entity c : clients)
			c.paintClients(g);
		for(Entity c : clients2)
			c.paintClients(g); 
		for(Entity c : clients3)
			c.paintClients(g);
	}
}
